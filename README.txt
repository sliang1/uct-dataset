---------------------------------------------------------------------------------
This dataset is about the clusters of users during a year, half a year, a quarter, a month, a week.
Folder 'data': users' tweets (each file contanis a single user's all tweets).
Folder 'groud-truth-clusters': each file is the clustering result during a time slice. 
		Data format (each line of each file): clusterLabel + \t + userId + \t + userId + ..
----------------------------------------------------------------------------------------